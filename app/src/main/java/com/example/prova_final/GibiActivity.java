package com.example.prova_final;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.prova_final.dto.GibiCompletoDTO;

public class GibiActivity extends AppCompatActivity {
    
    private TextView nome, resenha, numSerie, personagens, editora, genero, nacional, classificacao, anoPublicado, numPaginas, totalComentarios, precoPago, precoEstimado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gibi);
        
        bind();
        
        preencheInfo();
        
    }

    private void preencheInfo() {
        GibiCompletoDTO gibi = (GibiCompletoDTO) getIntent().getExtras().getSerializable("gibi");

        nome.setText(gibi.getNome());
        resenha.setText("Resenha: " + gibi.getResenha());
        numSerie.setText("Número de série: " + gibi.getNumeroSerie());
        personagens.setText("Personagens: " + gibi.getPersonagens());
        editora.setText("Editora: " + gibi.getEditora());
        genero.setText("Gênero: " + gibi.getGenero());
        nacional.setText((gibi.isNacional()) ? "Nacional" : "Estrangeiro");
        classificacao.setText("Classificação: " + gibi.getClassificacao());
        anoPublicado.setText("Ano publicado: " + gibi.getAnoPublicado());
        numPaginas.setText("Número de páginas: " + gibi.getNumPaginas());
        totalComentarios.setText("Total de comentários: " + gibi.getTotalComentarios());
        precoPago.setText("Preço pago: " + gibi.getPrecoPago());
        precoEstimado.setText("Preço estimado: " + gibi.getPrecoEstimado());
    }

    private void bind() {
        nome = findViewById(R.id.tvNomeGibi);
        resenha = findViewById(R.id.tvResenha);
        numSerie = findViewById(R.id.tvNumSerie);
        personagens = findViewById(R.id.tvPersonagens);
        editora = findViewById(R.id.tvEditora);
        genero = findViewById(R.id.tvGenero);
        nacional = findViewById(R.id.tvNacional);
        classificacao = findViewById(R.id.tvClassificacao);
        anoPublicado = findViewById(R.id.tvAnoPublicado);
        numPaginas = findViewById(R.id.tvNumPaginas);
        totalComentarios = findViewById(R.id.tvTotalComentarios);
        precoPago = findViewById(R.id.tvPrecoPago);
        precoEstimado = findViewById(R.id.tvPrecoEstimado);
    }
}