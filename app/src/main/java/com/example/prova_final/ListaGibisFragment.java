package com.example.prova_final;

import static android.content.Context.MODE_PRIVATE;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.prova_final.adapter.GibiAdapter;
import com.example.prova_final.dto.GibiDTO;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ListaGibisFragment extends Fragment {

    private RecyclerView rvGibi;

    @Override
    public void onStart() {
        super.onStart();

        ArrayList<GibiDTO> lista = (ArrayList<GibiDTO>) getArguments().getSerializable("gibis");
        rvGibi = getActivity().findViewById(R.id.rvGibi);

        String token = getArguments().getString("token");

        GibiAdapter g = new GibiAdapter(lista, viewGibi, token);
        LinearLayoutManager layout = new LinearLayoutManager(getActivity().getApplicationContext());
        rvGibi.setLayoutManager(layout);
        rvGibi.setAdapter(g);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lista_gibis, container, false);
    }

    ActivityResultLauncher<Intent> viewGibi = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if(result.getResultCode() == 10) {
                        SharedPreferences prefs = getActivity().getSharedPreferences("proj", MODE_PRIVATE);
                        String token = prefs.getString("token", "");


                    }
                }
            }
    );
}