package com.example.prova_final;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.prova_final.config.RetrofitConfig;
import com.example.prova_final.dto.InputUserDTO;
import com.example.prova_final.dto.UserDTO;
import com.example.prova_final.service.IGibiService;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity {

    private TextInputLayout tilUsuario, tilSenha;
    private Button btnLogin;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        
        bind();

        eventos();

    }

    private void eventos() {
        btnLogin.setOnClickListener(logar());
    }

    private View.OnClickListener logar() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validar(tilUsuario, tilSenha))
                    return;
                else {
                    String l = tilUsuario.getEditText().getText().toString();
                    String s = tilSenha.getEditText().getText().toString();

                    Retrofit r = new RetrofitConfig().getRetrofit();
                    IGibiService gibiServ = r.create(IGibiService.class);
                    InputUserDTO udto = new InputUserDTO("password", l, s);

                    Call<UserDTO> lg = gibiServ.login(udto.userAlic(),
                            udto.getGrant_type(),
                            udto.getUsername(),
                            udto.getPassword());

                    lg.enqueue(new Callback<UserDTO>() {
                        @Override
                        public void onResponse(Call<UserDTO> call, Response<UserDTO> response) {
                            if(response.isSuccessful()) {
                                if(response.body() != null) {
                                    Intent i = new Intent();
                                    UserDTO u = response.body();
                                    i.putExtra("user", u);
                                    setResult(10, i);

                                    SharedPreferences prefs = getSharedPreferences("proj", MODE_PRIVATE);
                                    SharedPreferences.Editor edt = prefs.edit();
                                    edt.putString("token", u.getAccess_token());
                                    edt.commit();

                                    finish();
                                } else {
                                    tilUsuario.getEditText().setText(null);
                                    tilSenha.getEditText().setText(null);
                                    Toast.makeText(getApplicationContext(), "Login ou senha incorretos", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<UserDTO> call, Throwable t) {

                        }
                    });
                }

            }
        };
    }

    private void bind() {
        tilUsuario = findViewById(R.id.tilUsuario);
        tilSenha = findViewById(R.id.tilSenha);
        btnLogin = findViewById(R.id.btnLogin);
    }

    private boolean validar(TextInputLayout login, TextInputLayout senha) {

        login.setError(null);
        senha.setError(null);

        String l = login.getEditText().getText().toString();
        String s = senha.getEditText().getText().toString();

        if (l.isEmpty() && s.isEmpty()) {
            login.setError("Esse campo deve ser preenchido!");
            senha.setError("Esse campo deve ser preenchido!");
            return true;
        } else if (l.isEmpty()) {
            login.setError("Esse campo deve ser preenchido!");
            return true;
        } else if (s.isEmpty()) {
            senha.setError("Esse campo deve ser preenchido!");
            return true;
        } else {
            return false;
        }

    }
}