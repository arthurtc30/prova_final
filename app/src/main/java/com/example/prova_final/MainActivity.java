package com.example.prova_final;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.ui.AppBarConfiguration;

import com.example.prova_final.config.RetrofitConfig;
import com.example.prova_final.databinding.ActivityMainBinding;
import com.example.prova_final.dto.GibiDTO;
import com.example.prova_final.dto.UserDTO;
import com.example.prova_final.service.IGibiService;
import com.example.prova_final.ui.home.HomeFragment;
import com.google.android.material.navigation.NavigationView;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.appBarMain.toolbar);

        DrawerLayout drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;
        Toolbar tool  = binding.appBarMain.toolbar;

        navigationView.setNavigationItemSelectedListener(clickNavigator());

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, tool,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        );

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        HomeFragment home = new HomeFragment();
        changeFragment(home);
    }

    private NavigationView.OnNavigationItemSelectedListener clickNavigator() {
        return new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                if(item.getItemId() == R.id.nav_login){
                    Intent itn = new Intent(getApplicationContext(), LoginActivity.class);
                    viewLogin.launch(itn);
                } else if (item.getItemId() == R.id.nav_logout) {
                    NavigationView nv = binding.navView;
                    nv.getMenu().clear();
                    nv.inflateMenu(R.menu.activity_main_drawer);

                    SharedPreferences prefs = getSharedPreferences("proj", MODE_PRIVATE);
                    SharedPreferences.Editor edt = prefs.edit();
                    edt.remove("token");
                    edt.commit();

                    changeFragment(new HomeFragment());
                } else if (item.getItemId() == R.id.nav_gibis) {
                    SharedPreferences prefs = getSharedPreferences("proj", MODE_PRIVATE);
                    String token = prefs.getString("token", "");

                    Retrofit r = new RetrofitConfig().getRetrofit();
                    IGibiService serv = r.create(IGibiService.class);

                    Call<List<GibiDTO>> lista = serv.listarGibis("Bearer " + token);
                    lista.enqueue(new Callback<List<GibiDTO>>() {
                        @Override
                        public void onResponse(Call<List<GibiDTO>> call, Response<List<GibiDTO>> response) {
                            if (response.isSuccessful()) {
                                if (response.body().size() > 0) {
                                    ListaGibisFragment lg = new ListaGibisFragment();
                                    Bundle args = new Bundle();
                                    args.putSerializable("gibis", (Serializable) response.body());
                                    args.putString("token", token);
                                    lg.setArguments(args);
                                    changeFragment(lg);
                                } else {
                                    Toast.makeText(getApplicationContext(),
                                            "Nenhum gibi encontrado",
                                            Toast.LENGTH_SHORT)
                                            .show();
                                    return;
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<List<GibiDTO>> call, Throwable t) {
                            Toast.makeText(getApplicationContext(),
                                    "DEU ERRO!!!",
                                    Toast.LENGTH_SHORT)
                                    .show();
                        }
                    });
                } else if (item.getItemId() == R.id.nav_home) {
                    changeFragment(new HomeFragment());
                }

                DrawerLayout drawer = binding.drawerLayout;
                drawer.closeDrawer(GravityCompat.START);

                return false;
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void changeFragment(Fragment frag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment_content_main, frag);
        transaction.commit();
    }

    ActivityResultLauncher<Intent> viewLogin = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {

                    if(result.getResultCode() == 10)
                    {
                        UserDTO userl = (UserDTO) result.getData().getExtras().getSerializable("user");

                        SharedPreferences prefs = getSharedPreferences("proj", MODE_PRIVATE);
                        SharedPreferences.Editor edt = prefs.edit();
                        edt.putString("token", userl.getAccess_token());
                        edt.commit();

                        Toast.makeText(getApplicationContext(), "Usuário logado", Toast.LENGTH_SHORT).show();

                        NavigationView nv = binding.navView;
                        nv.getMenu().clear();
                        nv.inflateMenu(R.menu.menu_logado_drawer);
                    }

                }
            }
    );

//    @Override
//    public boolean onSupportNavigateUp() {
//        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
//        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
//                || super.onSupportNavigateUp();
//    }
}