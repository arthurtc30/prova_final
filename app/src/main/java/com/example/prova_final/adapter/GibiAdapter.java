package com.example.prova_final.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.prova_final.R;
import com.example.prova_final.dto.GibiDTO;

import java.util.ArrayList;

public class GibiAdapter extends RecyclerView.Adapter<GibiHolder> {
    private ArrayList<GibiDTO> gibis;
    private ActivityResultLauncher<Intent> result;
    private String token;

    public GibiAdapter(ArrayList<GibiDTO> gibis, ActivityResultLauncher<Intent> result, String token) {
        this.gibis = gibis;
        this.result = result;
        this.token = token;
    }

    @NonNull
    @Override
    public GibiHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new GibiHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.linha_gibi, parent, false), result, token);
    }

    @Override
    public void onBindViewHolder(@NonNull GibiHolder holder, int position) {
        holder.preencher(gibis.get(position), position);
    }

    @Override
    public int getItemCount() {
        return gibis.size();
    }
}
