package com.example.prova_final.adapter;

import static android.app.PendingIntent.getActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.prova_final.GibiActivity;
import com.example.prova_final.ListaGibisFragment;
import com.example.prova_final.R;
import com.example.prova_final.config.RetrofitConfig;
import com.example.prova_final.dto.GibiCompletoDTO;
import com.example.prova_final.dto.GibiDTO;
import com.example.prova_final.service.IGibiService;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class GibiHolder extends RecyclerView.ViewHolder {

    private TextView nome, editora, ano, comentarios, numeroSerie, genero;
    private int position;
    private GibiDTO gibi;
    private ActivityResultLauncher<Intent> result;
    private String token;
    private GibiCompletoDTO gb;

    public GibiHolder(@NonNull View itemView, ActivityResultLauncher<Intent> result, String token) {
        super(itemView);
        bind();
        this.result = result;
        this.token = token;
        itemView.setOnClickListener(clique());
    }

    private View.OnClickListener clique() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Retrofit r = new RetrofitConfig().getRetrofit();
                IGibiService serv = r.create(IGibiService.class);

                Call<GibiCompletoDTO> lista = serv.pesquisarGibi("Bearer " + token, gibi.getId());
                lista.enqueue(new Callback<GibiCompletoDTO>() {
                    @Override
                    public void onResponse(Call<GibiCompletoDTO> call, Response<GibiCompletoDTO> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                Intent i = new Intent(view.getContext(), GibiActivity.class);
                                i.putExtra("gibi", response.body());
                                result.launch(i);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<GibiCompletoDTO> call, Throwable t) {

                    }
                });
            }
        };
    }

    private void bind() {
        nome = itemView.findViewById(R.id.linhaTvNome);
        editora = itemView.findViewById(R.id.linhaTvEditora);
        ano = itemView.findViewById(R.id.linhaTvAnoPublicado);
        comentarios = itemView.findViewById(R.id.linhaTvComentarios);
        numeroSerie = itemView.findViewById(R.id.linhaTvNumeroSerie);
        genero = itemView.findViewById(R.id.linhaTvGenero);
    }

    public void preencher(GibiDTO gibiDTO, int position) {
        this.gibi = gibiDTO;
        this.position = position;

        String aux1 = "Nome: " + gibiDTO.getNome();
        String aux2 = "Editora: " + gibiDTO.getEditora();
        String aux3 = "Ano: " + gibiDTO.getAnoPublicado();
        String aux4 = "Comentarios: " + gibiDTO.getTotalComentarios();
        String aux5 = "Número de série: " + gibiDTO.getNumeroSerie();
        String aux6 = "Genero: " + gibiDTO.getGenero();

        nome.setText(aux1);
        editora.setText(aux2);
        ano.setText(aux3);
        comentarios.setText(aux4);
        numeroSerie.setText(aux5);
        genero.setText(aux6);
    }
}
