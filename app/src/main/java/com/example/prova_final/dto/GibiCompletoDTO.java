package com.example.prova_final.dto;

import java.io.Serializable;

public class GibiCompletoDTO implements Serializable {
    private int id, classificacao, anoPublicado, numPaginas, totalComentarios, precoPago, precoEstimado;
    private String nome, resenha, numeroSerie, personagens, editora, genero;
    private boolean nacional;

    public GibiCompletoDTO() {
    }

    public GibiCompletoDTO(int id, int classificacao, int anoPublicado, int numPaginas, int totalComentarios, int precoPago, int precoEstimado, String nome, String resenha, String numSerie, String personagens, String editora, String genero, boolean nacional) {
        this.id = id;
        this.classificacao = classificacao;
        this.anoPublicado = anoPublicado;
        this.numPaginas = numPaginas;
        this.totalComentarios = totalComentarios;
        this.precoPago = precoPago;
        this.precoEstimado = precoEstimado;
        this.nome = nome;
        this.resenha = resenha;
        this.numeroSerie = numSerie;
        this.personagens = personagens;
        this.editora = editora;
        this.genero = genero;
        this.nacional = nacional;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(int classificacao) {
        this.classificacao = classificacao;
    }

    public int getAnoPublicado() {
        return anoPublicado;
    }

    public void setAnoPublicado(int anoPublicado) {
        this.anoPublicado = anoPublicado;
    }

    public int getNumPaginas() {
        return numPaginas;
    }

    public void setNumPaginas(int numPaginas) {
        this.numPaginas = numPaginas;
    }

    public int getTotalComentarios() {
        return totalComentarios;
    }

    public void setTotalComentarios(int totalComentarios) {
        this.totalComentarios = totalComentarios;
    }

    public int getPrecoPago() {
        return precoPago;
    }

    public void setPrecoPago(int precoPago) {
        this.precoPago = precoPago;
    }

    public int getPrecoEstimado() {
        return precoEstimado;
    }

    public void setPrecoEstimado(int precoEstimado) {
        this.precoEstimado = precoEstimado;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getResenha() {
        return resenha;
    }

    public void setResenha(String resenha) {
        this.resenha = resenha;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public String getPersonagens() {
        return personagens;
    }

    public void setPersonagens(String personagens) {
        this.personagens = personagens;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public boolean isNacional() {
        return nacional;
    }

    public void setNacional(boolean nacional) {
        this.nacional = nacional;
    }
}
