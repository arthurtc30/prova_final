package com.example.prova_final.dto;

import java.io.Serializable;

public class GibiDTO implements Serializable {
    private int id, anoPublicado, totalComentarios;
    private String nome, numeroSerie, editora, genero;

    public GibiDTO() {

    }

    public GibiDTO(int id, int anoPublicado, int totalComentarios, String nome, String numeroSerie, String editora, String genero) {
        this.id = id;
        this.anoPublicado = anoPublicado;
        this.totalComentarios = totalComentarios;
        this.nome = nome;
        this.numeroSerie = numeroSerie;
        this.editora = editora;
        this.genero = genero;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAnoPublicado() {
        return anoPublicado;
    }

    public void setAnoPublicado(int anoPublicado) {
        this.anoPublicado = anoPublicado;
    }

    public int getTotalComentarios() {
        return totalComentarios;
    }

    public void setTotalComentarios(int totalComentarios) {
        this.totalComentarios = totalComentarios;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
}
