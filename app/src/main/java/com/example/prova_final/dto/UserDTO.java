package com.example.prova_final.dto;

import java.io.Serializable;

public class UserDTO implements Serializable {
    private String access_token;
    private String login;

    public UserDTO() {
    }

    public UserDTO(String access_token, String login) {
        this.access_token = access_token;
        this.login = login;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
