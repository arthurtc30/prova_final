package com.example.prova_final.service;

import com.example.prova_final.dto.GibiCompletoDTO;
import com.example.prova_final.dto.GibiDTO;
import com.example.prova_final.dto.UserDTO;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IGibiService {
    @FormUrlEncoded
    @POST("/oauth/token")
    public Call<UserDTO> login(@Header("Authorization") String userApl,
                               @Field("grant_type") String grant_type,
                               @Field("username") String login,
                               @Field("password") String senha);

    @GET("/gibi")
    public Call<List<GibiDTO>> listarGibis(@Header("Authorization") String token);

    @GET("/gibi/{id}")
    public Call<GibiCompletoDTO> pesquisarGibi(@Header("Authorization") String token,
                                               @Path("id") int idGibi);
}
